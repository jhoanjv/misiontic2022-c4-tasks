package co.com.tasks.presenter;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import co.com.tasks.model.MainInteractor;
import co.com.tasks.mvp.MainMVP;
import co.com.tasks.view.dto.TaskItem;
import co.com.tasks.view.dto.TaskState;

public class MainPresenter implements MainMVP.Presenter {

    private final MainMVP.Model model;
    private final MainMVP.View view;

    public MainPresenter(MainMVP.View view) {
        this.view = view;
        model = new MainInteractor();
    }

    @Override
    public void loadTasks() {
        List<TaskItem> items = model.getTasks();

        view.showTaskList(items);
    }

    @Override
    public void addNewTask() {
        Log.i(MainPresenter.class.getSimpleName(), "Add new task");
        String description = view.getTaskDescription();
        String date = SimpleDateFormat.getDateTimeInstance().format(new Date());

        TaskItem task = new TaskItem(description, date);
        model.saveTask(task);
        view.addTaskToList(task);
    }

    @Override
    public void taskItemClicked(TaskItem task) {
        String message = task.getState() == TaskState.PENDING
                ? "Would you like to mark as DONE this task?"
                : "Would you like to mark as PENDING this task?";
        view.showconfirmDialog(message, task);

    }

    @Override
    public void updateTask(TaskItem task) {
        task.setState(task.getState() == TaskState.PENDING ? TaskState.DONE : TaskState.PENDING);
        model.updateTask(task);
        view.updateTask(task);
    }

    @Override
    public void taskItemLongClicked(TaskItem task) {
        if(task.getState()==TaskState.DONE){
            view.showDeleteDialog("Would you like to mark as REMOVE this task?", task);
        }

    }

    @Override
    public void deleteTask(TaskItem task) {
        model.deleteTask(task);
        view.deletTask(task);
    }


}
