package co.com.tasks.view.dto;

public enum TaskState {
    PENDING,
    DONE
}
