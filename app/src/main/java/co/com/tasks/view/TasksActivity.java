package co.com.tasks.view;

import androidx.appcompat.app.AppCompatActivity;
//import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
//import android.widget.Toast;

//import com.google.android.material.appbar.MaterialToolbar;
//import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import co.com.tasks.R;
import co.com.tasks.mvp.MainMVP;
import co.com.tasks.presenter.MainPresenter;
import co.com.tasks.view.adapter.TaskAdapter;
import co.com.tasks.view.dto.TaskItem;

public class TasksActivity extends AppCompatActivity implements MainMVP.View {

    private TextInputLayout tilNewTask;
    private TextInputEditText etNewTask;
    private RecyclerView rvTasks;

    private TaskAdapter taskAdapter;

    private MainMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        presenter = new MainPresenter(TasksActivity.this);

        initUI();
        presenter.loadTasks();
    }

    private void initUI() {
        tilNewTask = findViewById(R.id.til_new_task);
        tilNewTask.setEndIconOnClickListener(v -> presenter.addNewTask());

        etNewTask = findViewById(R.id.et_new_task);

        taskAdapter = new TaskAdapter();
        taskAdapter.setClickListener(item -> presenter.taskItemClicked(item));
        taskAdapter.setLongClickListener(item -> presenter.taskItemLongClicked(item));

        rvTasks = findViewById(R.id.rv_tasks);
        rvTasks.setLayoutManager(new LinearLayoutManager(TasksActivity.this));
        rvTasks.setAdapter(taskAdapter);
    }

    @Override
    public void showTaskList(List<TaskItem> items) {
        taskAdapter.setData((items));
    }

    @Override
    public String getTaskDescription() {
        return etNewTask.getText().toString();
    }

    @Override
    public void addTaskToList(TaskItem task) {
        taskAdapter.addItem(task);
    }

    @Override
    public void updateTask(TaskItem item) {
        taskAdapter.updateTask(item);
    }

    @Override
    public void showconfirmDialog(String message, TaskItem task) {
        new AlertDialog.Builder(this)
                .setTitle("Task selected")
                .setMessage(message)
                .setPositiveButton("yes", (DialogInterface.OnClickListener) (dialog, which) -> presenter.updateTask(task))
                .setNegativeButton("No",null)
                .show();
    }

    @Override
    public void showDeleteDialog(String message, TaskItem task) {
        new AlertDialog.Builder(this)
                .setTitle("Task selected")
                .setMessage(message)
                .setPositiveButton("yes", (DialogInterface.OnClickListener) (dialog, which) -> presenter.deleteTask(task))
                .setNegativeButton("No",null)
                .show();
    }

    @Override
    public void deletTask(TaskItem task) {
        taskAdapter.removeTask(task);
    }
}
