package co.com.tasks;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import co.com.tasks.view.TasksActivity;

public class SignUpActivity extends AppCompatActivity {

    private TextView tvDirectSignIn;
    private AppCompatButton btnSingUpAuth;
    private TextInputLayout tiEmailSignUp;
    private TextInputEditText etEmailSignUp;
    private TextInputLayout tiPassword1SignUp;
    private TextInputEditText etPassword1SignUp;
    private TextInputLayout tiPassword2SignUp;
    private TextInputEditText etPassword2SignUp;

    private String email = "";
    private String password = "";
    private String password2 = "";

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        tvDirectSignIn = findViewById(R.id.tv_direct_sign_in);
        tvDirectSignIn.setOnClickListener(v -> directSignInOnclick());

        //Register user new

        etEmailSignUp = findViewById(R.id.et_email_sign_up);
        etPassword1SignUp = findViewById(R.id.et_password_sign_up);
        etPassword2SignUp = findViewById(R.id.et_password_sign_up_confirm);
        btnSingUpAuth = findViewById(R.id.btn_sign_up_auth);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();


        btnSingUpAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = etEmailSignUp.getText().toString();
                password = etPassword1SignUp.getText().toString();
                password2 = etPassword2SignUp.getText().toString();


                if (!email.isEmpty() && !password.isEmpty() && !password2.isEmpty()) {

                    if (password.length() >= 6) {

                        if (password.equals(password2)) {

                            registerUser();

                        } else {
                            Toast.makeText(SignUpActivity.this, "Passwords are not same", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(SignUpActivity.this, "Password want 6 caracter", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SignUpActivity.this, "Empty space", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void registerUser() {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    Map<String, Object> map = new HashMap<>();
                    map.put("email", email);


                    String id = mAuth.getCurrentUser().getUid();

                    mDatabase.child("Users").child(id).setValue(map).addOnCompleteListener(task2 -> {
                        if (task2.isSuccessful()) {
                            startActivity(new Intent(SignUpActivity.this, TasksActivity.class));
                            finish();
                        } else {
                            Toast.makeText(SignUpActivity.this, "Data are not created", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(SignUpActivity.this, "User are not registered", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void directSignInOnclick() {
        Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
        startActivity(intent);
        finish();
    }
}