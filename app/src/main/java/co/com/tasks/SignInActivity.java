package co.com.tasks;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import co.com.tasks.view.TasksActivity;

public class SignInActivity extends AppCompatActivity {

    private TextView tvDirectSignUp;
    private TextInputLayout tiEmailSignIn;
    private TextInputEditText etEmailSignIn;
    private TextInputLayout tiPasswordSignIn;
    private TextInputEditText etPasswordSignIn;
    private AppCompatButton btnSignInAuth;

    private String email = "";
    private String password = "";

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        tvDirectSignUp= findViewById(R.id.tv_direct_sign_up);
        tvDirectSignUp.setOnClickListener(v -> directSignUpOnclick());

        //Ingreso de usuarios
        btnSignInAuth =findViewById(R.id.btn_sign_in_auth);
        etEmailSignIn = findViewById(R.id.et_email_sign_in);
        etPasswordSignIn = findViewById(R.id.et_password_sign_in);
        mAuth = FirebaseAuth.getInstance();

        btnSignInAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = etEmailSignIn.getText().toString();
                password = etPasswordSignIn.getText().toString();

                if (!email.isEmpty() && !password.isEmpty()){
                    logUser();
                }else{
                    Toast.makeText(SignInActivity.this, "Empty space", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void logUser() {
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    startActivity(new Intent(SignInActivity.this, TasksActivity.class));
                    finish();
                }else{
                    Toast.makeText(SignInActivity.this, "Email or password was not correct", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void directSignUpOnclick() {
        Intent intent = new Intent(SignInActivity.this,SignUpActivity.class);
        startActivity(intent);
        finish();
    }
}