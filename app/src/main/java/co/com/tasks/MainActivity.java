package co.com.tasks;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private AppCompatButton btnSignIn;
    private AppCompatButton btnSignUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSignIn= findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(v -> SignInOnclick());

        btnSignUp= findViewById(R.id.btn_sign_up);
        btnSignUp.setOnClickListener(v -> SignUpOnclick());

    }

    private void SignInOnclick() {
        Intent intent = new Intent(MainActivity.this,SignInActivity.class);
        startActivity(intent);
        finish();
    }

    private void SignUpOnclick() {
        Intent intent = new Intent(MainActivity.this,SignUpActivity.class);
        startActivity(intent);
        finish();
    }


}